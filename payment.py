# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from collections import defaultdict
from decimal import Decimal
from sql.aggregate import Count
from sql import Null
from trytond.report import Report
from trytond.i18n import gettext
from trytond.model import (ModelView, fields)
from trytond.pyson import Eval
from trytond.tools import reduce_ids, grouped_slice, cursor_dict
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, Button, StateTransition, StateReport
from trytond.pool import PoolMeta, Pool
from .exceptions import PaymentGroupProcessedError
from datetime import date

KINDS = [
    ('payable', 'Payable'),
    ('receivable', 'Receivable'),
]

_STATES = {
    'readonly': Eval('state') != 'draft',
}
_DEPENDS = ['state']

PAYMENT_TYPES = [
    ('1', 'Payment Supplier'),
    ('2', 'Payment Payroll'),
    ('3', 'Payment Party'),
]


def justify(string, size):
    return string[:size].ljust(size)


def format_decimal(n, include_sign=False):
    if not isinstance(n, Decimal):
        n = Decimal(n)
    sign = ''
    if include_sign:
        sign = 'N' if n < 0 else ''
    return sign + ('{0:.2f}'.format(abs(n))).replace('.', '').rjust(
        17 - len(sign), '0')


def format_integer(n, size=8):
    return ('%d' % int(n)).rjust(size, '0')


class Group(metaclass=PoolMeta):
    'Payment Group'
    __name__ = 'account.payment.group'

    voucher_generated = fields.Function(fields.Boolean(
        "Voucher Generated",
        help="The group was generated vouchers"),
        'get_voucher_generated')

    @classmethod
    def get_voucher_generated(cls, groups, names):
        Payment = Pool().get('account.payment')
        cursor = Transaction().connection.cursor()
        payment = Payment.__table__()

        # initialize result and columns
        result = defaultdict(defaultdict)
        columns = [
            payment.group.as_('group_id'),
            Count(payment.group,
                filter_=(payment.voucher != Null)).as_('count_voucher'),
        ]
        for sub_ids in grouped_slice(groups):
            cursor.execute(*payment.select(*columns,
                where=reduce_ids(payment.group, sub_ids),
                group_by=payment.group),
            )

            for row in cursor_dict(cursor):
                group_id = row['group_id']
                if row['count_voucher'] > 0:
                    result['voucher_generated'][group_id] = True
                else:
                    result['voucher_generated'][group_id] = False

        for key in list(result.keys()):
            if key not in names:
                del result[key]
        return result


class Payment(metaclass=PoolMeta):
    'Payment'
    __name__ = 'account.payment'
    voucher = fields.Many2One('account.voucher', 'Voucher', readonly=True,
        domain=[('party', 'in', [Eval('party', None), None]), ])

    @classmethod
    def succeed(cls, payments):
        if payments:
            for p in payments:
                if p.group.voucher_generated:
                    raise PaymentGroupProcessedError(
                        gettext('account_voucher.msg_payment_group_processed'))
        super(Payment, cls).succeed(payments)

    @classmethod
    def fail(cls, payments):
        if payments:
            for p in payments:
                if p.group.voucher_generated:
                    raise PaymentGroupProcessedError(
                        gettext('account_voucher.msg_payment_group_processed'))
        super(Payment, cls).fail(payments)

    @classmethod
    def _get_origin(cls):
        origins = super(Payment, cls)._get_origin()
        'Return list of Model names for origin Reference'
        return origins + ['account.invoice', 'account.voucher', 'account.note']


class GenerateVoucherStart(ModelView):
    'Generate Voucher Start'
    __name__ = 'account_payment.generate_voucher.start'
    payment_mode = fields.Many2One('account.voucher.paymode', 'Payment Mode',
        domain=[], required=True)
    advance_date = fields.Date('Advance Date', required=True)
    description = fields.Char('Description')


class GenerateVoucher(Wizard):
    'Generate Voucher'

    __name__ = 'account_payment.generate_voucher'
    start = StateView('account_payment.generate_voucher.start',
        'account_voucher.view_voucher_generate_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Generate', 'generate_', 'tryton-ok', default=True),
        ])
    generate_ = StateTransition()

    @classmethod
    def __setup__(cls):
        super(GenerateVoucher, cls).__setup__()

    def transition_generate_(self):
        pool = Pool()
        Voucher = pool.get('account.voucher')
        Group = pool.get('account.payment.group')
        active_id = Transaction().context['active_id']
        if not active_id:
            return

        group, = Group.browse([active_id])
        vouchers_to_create = {}
        payments_update = {}
        if group.payment_complete:
            for payment in group.payments:
                if payment.state == 'succeeded' and not payment.voucher:
                    party_id = payment.party.id
                    if not vouchers_to_create.get(party_id):
                        vouchers_to_create[party_id] = self.get_default_voucher(party_id)
                    if party_id not in payments_update.keys():
                        payments_update[party_id] = []
                    payments_update[party_id].append(payment)
                    line = self.get_default_line(payment)
                    vouchers_to_create[party_id]['lines'][0][1].append(line)
                    amount_to_pay = vouchers_to_create[party_id]['amount_to_pay']
                    vouchers_to_create[party_id]['amount_to_pay'] = amount_to_pay + line['amount']
            vouchers = Voucher.create(vouchers_to_create.values())
            for voucher in vouchers:
                Voucher.process([voucher])
                for p in payments_update[voucher.party.id]:
                    p.voucher = voucher.id
                    p.save()

        return 'end'

    def get_default_voucher(self, party_id):
        voucher = {
            'party': party_id,
            'voucher_type': 'payment',
            'date': self.start.advance_date,
            'description': self.start.description,
            'payment_mode': self.start.payment_mode.id,
            'state': 'draft',
            'account': self.start.payment_mode.account.id,
            'journal': self.start.payment_mode.journal.id,
            'method_counterpart': 'one_line',
            'amount_to_pay': Decimal(0.0),
            'lines': [('create', [])]
        }
        return voucher

    def get_default_line(self, payment):
        detail = None
        if payment.origin:
            Model = Pool().get('ir.model')
            name_model = Model.get_name(payment.origin.__name__)
            detail = str(name_model + ' ' + payment.origin.rec_name)
        line = {
            'detail': detail,
            'amount': payment.amount,
            'move_line': payment.line.id,
            'account': payment.line.account.id,
        }
        return line


class ProcessPayment(metaclass=PoolMeta):
    'Process Payment'
    __name__ = 'account.payment.process'

    def do_process(self, action):
        payments = self.records
        is_payment_group = False
        for payment in payments:
            if payment.group:
                is_payment_group = True
                break
        if not is_payment_group:
            super(ProcessPayment, self).do_process(action)


class GroupPaymentBankStart(ModelView):
    'Group Payment Bank Start'
    __name__ = 'account.group_payment_bank.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    report = fields.Many2One('ir.action.report', 'Report',
        domain=[('report_name', 'ilike', 'account.group_payment_bank.%')],
        required=True)
    payment_type = fields.Selection(PAYMENT_TYPES, 'Payment Types')
    bank = fields.Many2One('bank', 'Bank', states={'required': True})
    groups = fields.Many2Many('account.payment.group', None, None, "Group Payments")
    vouchers = fields.Many2Many('account.voucher', None, None, "Vouchers", 
        domain=[('voucher_type', '=', 'payment')])
    multipayments = fields.Many2Many('account.voucher', None, None, 
        "Multipayments", domain=[('voucher_type', '=', 'multipayment')])
    account_bank = fields.Many2One('bank.account.number', 'Account Bank',
        domain=[('account.bank', '=', Eval('bank'))], states={'required': True})
    description = fields.Char('Description')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @fields.depends('company', 'bank', 'account_bank')
    def on_change_bank(self):
        if self.bank and self.company:
            account_bank = []
            for account in self.company.party.bank_accounts:
                if account.bank == self.bank:
                    account_bank.append(account)
            if len(account_bank) > 0:
                self.account_bank = account_bank[0].numbers[0]



class GroupPaymentBank(Wizard):
    'Payroll Payment Bank'
    __name__ = 'account.group_payment_bank'
    start = StateView('account.group_payment_bank.start',
        'account_voucher.group_payment_bank_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('account.group_payment_bank.report')

    def do_print_(self, action):
        data = {
            'company': self.start.company.id,
            'report': self.start.report.id,
            'vouchers': [v.id for v in self.start.vouchers],
            'groups': [g.id for g in self.start.groups if g.payment_complete],
            'multipayments': [v.id for v in self.start.multipayments],
            'account_bank': self.start.account_bank.id,
            'payment_type': self.start.payment_type,
            'bank': self.start.bank.id,
            'description': self.start.description
        }

        action['report'] = self.start.report.report
        action['report_name'] = self.start.report.report_name
        action['id'] = self.start.report.id
        action['action'] = self.start.report.action.id
        return action, data

    def transition_print_(self):
        return 'end'


class GroupPaymentBankReport(Report):
    __name__ = 'account.group_payment_bank.report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Payment = pool.get('account.payment')
        Voucher = pool.get('account.voucher')
        Company = pool.get('company.company')
        AccountBank = pool.get('bank.account.number')
        Bank = pool.get('bank')
        bank = Bank(data['bank'])
        record_dict = {}
        sum_net_payment = []
        net_payment_append = sum_net_payment.append
        company = Company.search_read([('id', '=', data['company'])],
                fields_names=['party.id_number', 'party.name'])
        if data['groups']:
            domain = [
                ('company', '=', data['company']),
                ('group', 'in', data['groups']),
                ('state', '=', 'succeeded')
            ]
            fields_names = [
                'party.id_number', 'party.name', 'amount', 'party.bank_name',
                'party.bank_account', 'party.bank_account_type', 'party.type_document',
                'party.check_digit', 'party.first_name', 'party.second_name',
                'party.first_family_name', 'party.second_family_name'
            ]
            records = Payment.search_read(domain, fields_names=fields_names)

            for rec in records:
                party_id = rec['party.']['id']
                try:
                    record_dict[party_id]['amount'] += rec['amount']
                    net_payment_append(rec['amount'])
                except Exception:
                    record_dict[party_id] = {
                        'amount': rec['amount'],
                        'party_name': rec['party.']['name'],
                        'party_first_name': rec['party.']['first_name'],
                        'party_second_name': rec['party.']['second_name'],
                        'party_first_family_name': rec['party.']['first_family_name'],
                        'party_second_family_name': rec['party.']['second_family_name'],
                        'id_number': rec['party.']['id_number'],
                        'type_document': rec['party.']['type_document'],
                        'bank_name': rec['party.']['bank_name'],
                        'bank_account': rec['party.']['bank_account'],
                        'check_digit': rec['party.']['check_digit'],
                        'bank_account_type': rec['party.']['bank_account_type'],
                    }
                    net_payment_append(rec['amount'])
        if data.get('vouchers'):
            fields_vouchers = ['party.id_number', 'party.name', 'amount_to_pay',
                'party.first_name', 'party.second_name',
                'party.first_family_name', 'party.second_family_name',
                'party.bank_name', 'party.bank_account', 'party.bank_account_type',
                'party.type_document', 'party.bank', 'party.check_digit']
            vouchers = Voucher.search_read([('id', 'in', data['vouchers'])],
                fields_names=fields_vouchers)
            for rec in vouchers:
                party_id = rec['party.']['id']
                try:
                    record_dict[party_id]['amount'] += rec['amount_to_pay']
                    net_payment_append(rec['amount_to_pay'])
                except Exception:
                    record_dict[party_id] = {
                        'amount': rec['amount_to_pay'],
                        'party_name': rec['party.']['name'],
                        'party_first_name': rec['party.']['first_name'],
                        'party_second_name': rec['party.']['second_name'],
                        'party_first_family_name': rec['party.']['first_family_name'],
                        'party_second_family_name': rec['party.']['second_family_name'],
                        'id_number': rec['party.']['id_number'],
                        'check_digit': rec['party.']['check_digit'],
                        'type_document': rec['party.']['type_document'],
                        'bank_name': rec['party.']['bank_name'],
                        'bank': rec['party.']['bank'],
                        'bank_account': rec['party.']['bank_account'],
                        'bank_account_type': rec['party.']['bank_account_type'],
                    }
                    net_payment_append(rec['amount_to_pay'])

        if data.get('multipayments'):
            fields_vouchers = ['lines.party.id_number', 'lines.party.name',
                'lines.party.first_name', 'lines.party.second_name',
                'lines.party.first_family_name', 'lines.party.second_family_name',
                'lines.amount', 'lines.party.bank_name', 'lines.party.bank_account',
                'lines.party.bank_account_type', 'lines.party.type_document',
                'lines.account.type.statement', 'lines.party.bank',
                'lines.party.check_digit']
            multipayments = Voucher.search_read(
                [('id', 'in', data['multipayments'])],
                fields_names=fields_vouchers)
            for rec in multipayments:
                for line in rec['lines.']:
                    if line['account.']['type.']['statement'] != 'balance':
                        continue
                    party_id = line['party.']['id']
                    try:
                        record_dict[party_id]['amount'] += line['amount']
                        net_payment_append(line['amount'])
                    except Exception:
                        record_dict[party_id] = {
                            'amount': line['amount'],
                            'party_name': line['party.']['name'],
                            'party_first_name': line['party.']['first_name'],
                            'party_second_name': line['party.']['second_name'],
                            'party_first_family_name': line['party.']['first_family_name'],
                            'party_second_family_name': line['party.']['second_family_name'],
                            'id_number': line['party.']['id_number'],
                            'check_digit': line['party.']['check_digit'],
                            'type_document': line['party.']['type_document'],
                            'bank_name': line['party.']['bank_name'],
                            'bank': line['party.']['bank'],
                            'bank_account': line['party.']['bank_account'],
                            'bank_account_type': line['party.']['bank_account_type'],
                        }
                        net_payment_append(line['amount'])

        report_context['records'] = list(record_dict.values())
        report_context['company'] = company[0]
        report_context['bank'] = bank
        report_context['account_bank'] = AccountBank(data['account_bank'])
        report_context['sum_net_payment'] = sum(sum_net_payment)
        report_context['number_records'] = len(report_context['records'])
        report_context['justify'] = justify
        report_context['format_integer'] = format_integer
        report_context['format_decimal'] = format_decimal
        return report_context


class BankReportBancolombia(GroupPaymentBankReport):
    __name__ = 'account.group_payment_bank.report_bancolombia'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header,  data)
        report_context['type_document'] = {
            '13': '1',
            '22': '2',
            '31': '3',
        }
        report_context['type_transaction'] = {
            'checking_account': '27',
            'saving_account': '37',
        }
        type_origin = {
            'checking_account': 'D',
            'saving_account': 'S',
        }
        report_context['type_transaction_origin'] = type_origin[report_context['account_bank'].type]

        payment_types = {
            '1': '220',
            '2': '225',
            '3': '238',
        }
        report_context['payment_type'] = payment_types[data['payment_type']]
        report_context['today'] = date.today().strftime('%y%m%d')
        print(report_context, 'validate')
        return report_context


class BankReportBogota(GroupPaymentBankReport):
    __name__ = 'account.group_payment_bank.report_bogota'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        report_context['type_document'] = {
            '13': 'C',
            '22': 'E',
            '31': 'N',
        }
        report_context['type_transaction'] = {
            'checking_account': '1',
            'saving_account': '2',
        }

        report_context['today'] = date.today()
        print(report_context, 'this is report context')
        return report_context


class BankReportDavivienda(GroupPaymentBankReport):
    __name__ = 'account.group_payment_bank.report_davivienda'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        report_context['type_document'] = {
            '13': '01',
            '22': '02',
            '31': '03',
        }
        report_context['type_transaction'] = {
            'checking_account': 'Cuenta corriente',
            'saving_account': 'Cuenta de ahorros',
        }

        report_context['today'] = date.today()
        print(report_context, 'this is report context')
        return report_context


class BankReportOccidente(GroupPaymentBankReport):
    __name__ = 'account.group_payment_bank.report_occidente'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        report_context['type_document'] = {
            '13': 'Persona Natural',
            '22': 'Persona Extranjera',
            '31': 'Persona Jurídica',
        }

        report_context['type_transaction'] = {
            'checking_account': 'Cuenta Corriente',
            'saving_account': 'Cuenta Ahorros',
        }

        report_context['payment_mode'] = {
            "1": "Abono cuenta Banco de Occidente",
            "0": "Abono cuenta otras entidades"
        }
        today = date.today()
        report_context['year'] = today.year
        report_context['month'] = format_integer(today.month, 2)
        report_context['day'] = format_integer(today.day, 2)
        print(report_context, 'this is report context')
        return report_context
