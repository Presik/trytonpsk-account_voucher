# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal

from trytond.model import ModelView, ModelSQL, fields, Workflow
from trytond.wizard import Wizard, StateTransition
from trytond.transaction import Transaction
from trytond.pyson import Eval, Bool
from trytond.pool import Pool
from trytond.report import Report
from trytond.exceptions import UserError
from trytond.i18n import gettext

conversor = None
try:
    from numword import numword_es
    conversor = numword_es.NumWordES()
except:
    print("Warning: Does not possible import numword module, please install it...!")


STATES = {
    'readonly': Eval('state') != 'draft',
}


class MultiRevenue(Workflow, ModelSQL, ModelView):
    'MultiRevenue'
    __name__ = 'account.multirevenue'
    _rec_name = 'code'
    code = fields.Char('Code', readonly=True)
    date = fields.Date('Date', required=True, states=STATES)
    company = fields.Many2One('company.company', 'Company',
        required=True, states=STATES)
    party = fields.Many2One('party.party', 'Party', required=True,
        states=STATES)
    transactions = fields.One2Many('account.multirevenue.transaction', 'multirevenue', 'Transactions',  states=STATES)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('processed', 'Processed'),
        ('cancel', 'Cancel'),
        ], 'State', select=True, readonly=True)
    total_transaction = fields.Function(fields.Numeric('Total Transaction',
        readonly=True), 'get_total_transaction')
    total_lines_to_pay = fields.Function(fields.Numeric('Total Lines to Pay',
        readonly=True), 'get_total_amount_line')
    new_balance = fields.Function(fields.Numeric('New Balance', readonly=True),
        'get_new_balance')
    total_line_receipt = fields.Function(fields.Numeric('Total Line Receipt',
        readonly=True), 'get_total_line_receipt')
    diference = fields.Function(fields.Numeric('Diference', readonly=True),
        'get_diference')
    lines = fields.One2Many('account.multirevenue.line', 'multirevenue',
        'Lines', states=STATES)

    @classmethod
    def __setup__(cls):
        super(MultiRevenue, cls).__setup__()
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state') == 'draft',
                },
            'process': {
                'invisible': Eval('state') != 'draft',
                },
            'cancel': {
                'invisible': Eval('state') != 'draft',
            },
        })
        cls._transitions |= set((
            ('draft', 'processed'),
            ('draft', 'cancel'),
            ('processed', 'draft'),
            ('processed', 'cancel'),
            ('cancel', 'draft'),
        ))

        cls._buttons.update({
            'get_lines_to_pay': {
                'invisible': Eval('state') != 'draft',
            },
            'generate_vouchers': {
                'invisible': Eval('state') != 'processed',
            },
        })

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, records):
        pool = Pool()
        MultiRevenueLineVouchers = pool.get('account.multirevenue.line.vouchers')
        Voucher = pool.get('account.voucher')
        for record in records:
            for line in record.lines:
                for v in line.vouchers:
                    Voucher.cancel([v.voucher])
                    MultiRevenueLineVouchers.delete([v])

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('processed')
    def process(cls, records):
        for record in records:
            if not record.validate_total_amount():
                raise UserError(gettext('account_voucher.msg_total_error'))
            if not record.code:
                cls.set_number(record)

    def validate_total_amount(self):
        if self.total_transaction == self.total_line_receipt:
            return True
        return False

    @classmethod
    @ModelView.button
    def generate_vouchers(cls, records):
        for record in records:
            record.validate_vouchers()
            record.create_voucher()

    @classmethod
    @ModelView.button
    def get_lines_to_pay(cls, records):
        MoveLine = Pool().get('account.move.line')
        MultiRevenueLines = Pool().get('account.multirevenue.line')
        for record in records:
            if record.lines:
                record.validate_vouchers()
                record.delete_lines()
            movelines = MoveLine.search([
                ('move.state', '=', 'posted'),
                ('move.company', '=', record.company.id),
                ('party', '=', record.party.id),
                ('state', '=', 'valid'),
                ('account.reconcile', '=', True),
                ['OR',
                    ('account.type.receivable', '=', True),
                    ('account.type.payable', '=', True),
                ],
                ('reconciliation', '=', None),
            ], order=[('move.date', 'ASC')])
            lines_to_create = []
            for line in movelines:
                sum_lines = 0
                multirevenue_lines = MultiRevenueLines.search([
                    ('move_line', '=', line.id)
                ])
                if multirevenue_lines:
                    sum_lines = sum([line.amount for line in multirevenue_lines
                                     if line.amount and line.vouchers])
                if line.move_origin and line.move_origin.__name__ == 'account.invoice':
                    original_amount_ = line.move_origin.amount_to_pay
                else:
                    original_amount_ = line.amount
                    original_amount_ = original_amount_ - sum_lines

                lines_to_create.append({
                    'multirevenue': record.id,
                    'move_line': line.id,
                    'reference_document': line.reference or '',
                    'original_amount': original_amount_,
                    'amount': 0,
                })
            MultiRevenueLines.create(lines_to_create)

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['lines'] = None
        default['state'] = 'draft'
        default['code'] = None
        return super(MultiRevenue, cls).copy(records, default=default)

    def delete_lines(self):
        MultiRevenueLines = Pool().get('account.multirevenue.line')
        MultiRevenueLines.delete(self.lines)

    def validate_vouchers(self):
        for line in self.lines:
            if line.vouchers:
                raise UserError(gettext('account_voucher.msg_exist_voucher'))

    def get_total_transaction(self, name):
        amount_transaction = []
        if self.transactions:
            for tr in self.transactions:
                amount_transaction.append(tr.amount)
            return sum(amount_transaction)
        else:
            return 0

    def create_payments(self):
        if self.lines:
            self.validate_vouchers()
        else:
            return
        amount_ = 0
        for transaction in self.transactions:
            i = self.transactions.index(transaction)
            amount_ = transaction.amount
            self.compute_payment_lines(amount_, transaction)
            if i == (len(self.transactions) - 1) and self.new_balance < 0:
                self.create_new_line(None, abs(self.new_balance), Decimal('0.00'), transaction)

    def compute_payment_lines(self, amount_, transaction):
        for line in self.lines:
            if line.amount:
                continue
            if amount_ >= line.original_amount:
                amount_ -= line.original_amount
                value_ = line.original_amount
                self.update_line(line, value_, transaction)
            else:
                value_ = amount_
                amount_ = 0
                self.update_line(line, value_, transaction)
                new_balance = line.original_amount - value_
                self.create_new_line(line.move_line, 0, new_balance, None)

            if amount_ == 0:
                break

    def update_line(self, line, value, transaction):
        line.write([line], {'amount': value})

    def create_new_line(self, move_line, amount_, original_amount,
                        transaction):
        MultiRevenueLine = Pool().get('account.multirevenue.line')
        move_line_id = move_line.id if move_line else None
        reference = move_line.reference if move_line else None
        create_line = {
            'multirevenue': self.id,
            'move_line': move_line_id,
            'amount': amount_,
            'original_amount': original_amount,
            'is_prepayment': False,
            'reference_document': reference,
        }
        if transaction:
            create_line['is_prepayment'] = True
        line, = MultiRevenueLine.create([create_line])
        return line

    def create_voucher(self):
        voucher_to_create = {}
        pool = Pool()
        Voucher = pool.get('account.voucher')
        MultiRevenueLine = pool.get('account.multirevenue.line')
        MultiRevenueLineVouchers = pool.get('account.multirevenue.line.vouchers')
        voucher_type = 'receipt'
        lines_created = {}
        line_paid = []
        concept_ids = []
        for transaction in self.transactions:
            payment_mode = transaction.payment_mode
            amount_tr = transaction.amount
            lines_created[transaction.id] = {'ids': []}
            for line in self.lines:
                if line.id in line_paid or not line.amount:
                    continue

                if transaction.id not in voucher_to_create.keys():
                    voucher_to_create[transaction.id] = {
                        'party': self.party.id,
                        'company': line.multirevenue.company.id,
                        'voucher_type': voucher_type,
                        'date': transaction.date,
                        'description': '',
                        'reference': '',
                        'payment_mode': payment_mode.id,
                        'state': 'draft',
                        'account': payment_mode.account.id,
                        'journal': payment_mode.journal.id,
                        'lines': [('create', [])],
                        'method_counterpart': 'one_line',
                    }

                if payment_mode.bank_account and payment_mode.bank_account:
                    voucher_to_create[transaction.id]['bank'] = payment_mode.bank_account.bank.id
                concept_amounts = 0
                for concept in line.others_concepts:
                    if concept.id in concept_ids:
                        continue
                    voucher_to_create[transaction.id]['lines'][0][1].append({
                        'detail': concept.description,
                        'amount': concept.amount,
                        'account':  concept.account.id,
                    })
                    concept_amounts += concept.amount
                    concept_ids.append(concept.id)
                net_payment = line.amount + concept_amounts
                if net_payment < amount_tr:
                    _line_amount = line.amount
                    amount_tr -= net_payment
                    line_paid.append(line.id)
                else:
                    _line_amount = amount_tr + abs(concept_amounts)
                    line.amount = line.amount - _line_amount
                    amount_tr = 0
                if line.move_line:
                    move_line = line.move_line
                    voucher_to_create[transaction.id]['description'] = 'PAGO DE FACTURAS'
                    voucher_to_create[transaction.id]['reference'] = line.multirevenue.code or ''

                    origin_number = line.origin.number if line.origin else None
                    total_amount = line.origin.total_amount if line.origin and line.origin.__name__ == 'account.invoice' else move_line.amount

                    voucher_to_create[transaction.id]['lines'][0][1].append({
                        'detail': origin_number or move_line.reference or move_line.description,
                        'amount': _line_amount,
                        'amount_original': total_amount,
                        'move_line': move_line.id,
                        'account': move_line.account.id,
                    })
                else:
                    detail_ = line.reference_document
                    account_id = line.account.id if line.account else None
                    if not account_id:
                        raise UserError(gettext('account_voucher.msg_without_account'))
                    if line.is_prepayment:
                        detail_ = 'ANTICIPO'

                    voucher_to_create[transaction.id]['lines'][0][1].append({
                        'detail': detail_,
                        'amount': _line_amount,
                        'account':  account_id,
                    })
                lines_created[transaction.id]['ids'].append(line.id)
                if amount_tr == 0:
                    break
        for key in voucher_to_create.keys():
            voucher, = Voucher.create([voucher_to_create[key]])
            voucher.on_change_lines()
            voucher.save()
            line_mult = MultiRevenueLine.search([
                ('id', 'in', lines_created[key]['ids'])
            ])
            for l in line_mult:
                v_create = {
                    'multirevenue_line': l.id,
                    'voucher': voucher.id
                }
                MultiRevenueLineVouchers.create([v_create])
            Voucher.process([voucher])
            Voucher.post([voucher])

    def get_total_amount_line(self, name):
        if self.lines:
            sum_amount = []
            for line in self.lines:
                if line.amount:
                    sum_amount.append(line.amount)
                else:
                    sum_amount.append(line.new_balance)
            return sum(sum_amount)

    def get_diference(self, name):
        return self.total_transaction - self.get_total_net_payment()

    def get_total_net_payment(self, name=None):
        net_payment = 0
        for line in self.lines:
            net_payment += line.net_payment
        return net_payment

    def get_new_balance(self, name):
        if self.total_lines_to_pay and self.total_transaction:
            return self.total_lines_to_pay - self.total_transaction

    def get_total_line_receipt(self, name):
        total_lines = 0
        for line in self.lines:
            if line.amount:
                total_lines += line.amount
            for c in line.others_concepts:
                total_lines += c.amount
        return total_lines

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or None

    @classmethod
    def validate(cls, records):
        pass

    @classmethod
    def set_number(cls, request):
        '''
        Fill the number field with the MultiRevenue sequence
        '''
        pool = Pool()
        Config = pool.get('account.voucher_configuration')
        config = Config.search([
            ('company', '=', request.company.id)
        ])
        if config:
            if not config[0].multirevenue_sequence:
                return
            number = config[0].multirevenue_sequence.get()
            cls.write([request], {'code': number})

    @classmethod
    def delete(cls, records):
        for record in records:
            record.validate_vouchers()
        return super(MultiRevenue, cls).delete(records)


class PaymentLines(Wizard):
    'Payment Lines'
    __name__ = 'account.multirevenue.payment_lines'
    start_state = 'payment_lines'
    payment_lines = StateTransition()

    def transition_payment_lines(self):
        MultiRevenue = Pool().get('account.multirevenue')
        ids = Transaction().context['active_ids']
        for mr in MultiRevenue.browse(ids):
            if mr.state != 'draft':
                return 'end'
            mr.create_payments()
        return 'end'


class MultiRevenueTransaction(ModelSQL, ModelView):
    'MultiRevenue Transaction'
    __name__ = 'account.multirevenue.transaction'
    _rec_name = 'amount'
    multirevenue = fields.Many2One('account.multirevenue', 'Multirevenue',
        required=True, select=True)
    description = fields.Char('Description')
    amount = fields.Numeric('Amount', required=True)
    date = fields.Date('Date', required=True)
    payment_mode = fields.Many2One('account.voucher.paymode', 'Payment Mode',
        required=True, domain=[
            ('company', '=', Eval('context', {}).get('company', -1)),
        ])
    state = fields.Function(fields.Char('State', readonly=True), 'get_state')

    @classmethod
    def __setup__(cls):
        super(MultiRevenueTransaction, cls).__setup__()
        cls._order.insert(0, ('date', 'ASC')),

    @classmethod
    def delete(cls, records):
        for record in records:
            record.multirevenue.validate_vouchers()
        return super(MultiRevenueTransaction, cls).delete(records)

    def get_state(self, name):
        if self.multirevenue:
            return self.multirevenue.state


class MultiRevenueOtherConcepts(ModelSQL, ModelView):
    'Others Concepts'
    __name__ = 'account.multirevenue.others_concepts'
    _rec_name = 'amount'
    multirevenue_line = fields.Many2One('account.multirevenue.line', 'Line',
        required=True, select=True)
    account = fields.Many2One('account.account', 'Account', required=True,
        domain=[
            ('company', '=', Eval('context', {}).get('company', -1)),
            ('type', '!=', None),
        ])
    amount = fields.Numeric('Amount', required=True)
    description = fields.Char('Description')


class MultiRevenueLineVouchers(ModelSQL, ModelView):
    'MultiRevenueLine - Vouchers'
    __name__ = 'account.multirevenue.line.vouchers'
    multirevenue_line = fields.Many2One('account.multirevenue.line', 'Line',
        required=True, select=True)
    voucher = fields.Many2One('account.voucher', 'Voucher', readonly=True)


class MultiRevenueLine(ModelSQL, ModelView):
    'MultiRevenue Line'
    __name__ = 'account.multirevenue.line'
    multirevenue = fields.Many2One('account.multirevenue', 'Multirevenue',
        required=True, select=True)
    move_line = fields.Many2One('account.move.line', 'Move Line', domain=[
            ('move.state', '=', 'posted'),
            ('move.company', '=', Eval('context', {}).get('company', -1)),
            ('state', '=', 'valid'),
            ('account.reconcile', '=', True),
            ['OR',
                ('account.type.receivable', '=', True),
                ('account.type.payable', '=', True),
            ],
            ('reconciliation', '=', None),
    ], depends=['party'])
    date_document = fields.Function(fields.Date('Date Document',
        readonly=True), 'get_data')
    reference_document = fields.Char('Reference')
    party_document = fields.Function(fields.Many2One('party.party',
        'Party'), 'get_party')
    original_amount = fields.Numeric('Original Amount', readonly=True)
    amount = fields.Numeric('Amount')
    concept_amounts = fields.Function(fields.Numeric('Others Concepts',
        readonly=True), 'get_concept_amounts')
    net_payment = fields.Function(fields.Numeric('Net Payment',
        readonly=True), 'get_net_payment')
    new_balance = fields.Function(fields.Numeric('Balance',
        readonly=True), 'get_balance')
    origin = fields.Function(fields.Reference('Origin',
        selection='get_origin_selection',  readonly=True), 'get_origin')
    state = fields.Function(fields.Char('State', readonly=True), 'get_state')
    is_prepayment = fields.Boolean('is_prepayment')
    others_concepts = fields.One2Many('account.multirevenue.others_concepts',
                                      'multirevenue_line', 'Others Concepts')
    account = fields.Many2One('account.account', 'Account', states={
        'invisible': Bool(Eval('move_line')),
        }, domain=[
            ('company', '=', Eval('context', {}).get('company', -1)),
            ('type', '!=', None),
        ])
    vouchers = fields.One2Many('account.multirevenue.line.vouchers',
        'multirevenue_line', 'Vouchers', readonly=True)
    vouchers_string = fields.Function(fields.Char('Vouchers'), 'get_vouchers_string')

    @classmethod
    def __setup__(cls):
        super(MultiRevenueLine, cls).__setup__()
        cls._order.insert(0, ('move_line', 'ASC'))

    @classmethod
    def delete(cls, records):
        for record in records:
            record.multirevenue.validate_vouchers()
        return super(MultiRevenueLine, cls).delete(records)

    @fields.depends('move_line', 'original_amount', 'reference_document')
    def on_change_move_line(self):
        if self.move_line:
            self.original_amount = self.move_line.move_origin.amount_to_pay \
                if self.move_line.move_origin and self.move_line.move_origin.__name__ == 'account.invoice' \
                else self.move_line.amount
            self.reference_document = self.move_line.move_origin.number \
                if self.move_line.move_origin and self.move_line.move_origin.__name__ == 'account.invoice' \
                else self.move_line.reference

    def get_vouchers_string(self, name):
        string_ = ''
        for v in self.vouchers:
            string_ += v.voucher.number + ' '
        return string_

    def get_data(self, name):
        if name == 'date_document' and self.move_line:
            return self.move_line.move.date

    def get_state(self, name):
        if self.multirevenue:
            return self.multirevenue.state

    def get_party(self, name):
        if self.multirevenue and self.multirevenue.party:
            return self.multirevenue.party.id

    def get_balance(self, name):
        if self.amount and self.original_amount:
            return abs(self.original_amount - self.amount)
        else:
            return abs(self.original_amount)

    def get_concept_amounts(self, name):
        if self.others_concepts:
            return sum([c.amount for c in self.others_concepts])
        else:
            return 0

    def get_net_payment(self, name):
        return (self.amount or 0) + (self.concept_amounts or 0)

    @classmethod
    def get_origin_selection(cls):
        Move = Pool().get('account.move')
        return Move.get_origin()

    def get_origin(self, name):
        if self.move_line and self.move_line.move_origin:
            return str(self.move_line.move_origin)

    @staticmethod
    def default_original_amount():
        return 0

    @staticmethod
    def default_is_prepayment():
        return False


class MultiRevenueReport(Report):
    'MultiRevenue Report'
    __name__ = 'account.multirevenue.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(MultiRevenueReport, cls).get_context(records, data)
        Company = Pool().get('company.company')
        company_id = Transaction().context.get('company')
        report_context['company'] = Company(company_id)
        return report_context
